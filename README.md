# IOC for DTL-050 vacuum turbopumps

## Used modules

*   [vac_ctrl_tcp350](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_tcp350)


## Controlled devices

*   DTL-050:Vac-VEPT-01100
    *   DTL-050:Vac-VPT-01100
